defmodule FluxErrorTest do
  use ExUnit.Case, async: true

  import ExUnit.CaptureLog, only: [capture_log: 1]

  describe "from/3" do
    test "generate FluxError based on atom" do
      error = :i_failed
      reason = :external_failure
      metadata = []

      result = %FluxError{
        reason: :external_failure,
        metadata: %{detail: :i_failed}
      }

      assert result == FluxError.from(error, reason, metadata)
    end

    test "generate FluxError based on map" do
      error = %{reason: :i_failed}
      reason = :external_failure
      metadata = []

      result = %FluxError{
        reason: :external_failure,
        metadata: %{detail: :i_failed}
      }

      assert result == FluxError.from(error, reason, metadata)
    end
  end

  describe "log/2" do
    test "log non-flux error with default message" do
      operation = fn ->
        try do
          throw(:i_failed)
        catch
          error ->
            stacktrace = __STACKTRACE__
            assert %FluxError{} = FluxError.log(error, stacktrace)
        end
      end

      assert capture_log(operation) =~ "triggered unhandled error"
    end
  end

  describe "to_map/2" do
    test "convert to JSON-friendly map" do
      error = FluxError.new(:unknown_error)

      opts = [format: :python]

      result = %{
        error: true,
        id: :unknown_error,
        message: "triggered unhandled error",
        message_raw: "triggered unhandled error",
        metadata: %{}
      }

      assert result == FluxError.to_map(error, opts)
    end
  end
end
