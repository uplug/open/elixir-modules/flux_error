defmodule FluxError.DocTest do
  use ExUnit.Case, async: true

  @moduletag :capture_log

  doctest FluxError
end
