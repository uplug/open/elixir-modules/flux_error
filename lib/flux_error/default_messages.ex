defmodule FluxError.DefaultMessages do
  @moduledoc """
  The default message generator module.
  """
  @moduledoc since: "0.0.1"

  use FluxError.Messages

  @impl FluxError.Messages
  @spec get_message(atom, keyword) :: String.t()
  def get_message(_reason, _opts) do
    "triggered unhandled error"
  end
end
