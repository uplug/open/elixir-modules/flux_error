defmodule FluxError.Messages do
  @moduledoc """
  Module responsible to extract error messages.

  The main approach to extract is done by defining a module which extends
  `FluxError.Messages` and implements `c:get_messages/2` function. For example:

  ```elixir
  defmodule MyApp.ErrorMessages do
    use FluxError.Messages

    @impl FluxError.Messages
    def get_message(:invalid_type, opts) do
      "the type '" <> key(:type, opts) <> "' is invalid. " <>
        "Valid types are: " <> key(:valid_types, opts)
    end

    def get_message(:missing_type, _opts) do
      "the payload must have the key 'type'"
    end

    def get_message(_reason, _opts) do
      "unknown error"
    end
  end
  ```

  The example above will generate 3 messages:

  - If `:reason` is `:invalid_type`, it will generate the following message
    (with default options):`The type '%{type}' is invalid. Valid types are:
    %{valid_types}`

  - If `:reason` is `:missing_type`, it will generate the following message:
    `the payload must have the key 'type'`

  - Otherwise, it will generate the default message: `unknown error`

  Using `FluxError.message/1` with error `:reason` as `:invalid_type` and
  `:metadata` containing both `:type` and `:valid_types` keys. It will inject
  the metadata values into the message generated.

  The `key/2` function generate the injection string based on `:format` opts:

  - `:elixir` - As defined by `Gettext`.

  - `:python` - As defined by [Python's
    Gettext](https://docs.python.org/3/library/gettext.html)

  - `:ruby` - As defined by [Ruby's
    Gettext](https://www.rubydoc.info/gems/gettext/3.2.9)

  - Defaults to the Elixir standard.

  You can create additional functions and options to aid the generation of
  messages.
  """
  @moduledoc since: "0.0.1"

  defmacro __using__(_opts) do
    quote do
      @behaviour FluxError.Messages

      @spec get(atom, keyword) :: String.t()
      def get(reason, opts \\ []) do
        get_message(reason, opts)
      end

      @spec key(atom | String.t(), keyword) :: String.t()
      def key(name, opts) do
        case Keyword.get(opts, :format) do
          :elixir -> "%{#{name}}"
          :python -> "%(#{name})s"
          :ruby -> "%{#{name}}"
          _format -> "%{#{name}}"
        end
      end
    end
  end

  @callback get_message(reason :: atom, opts :: keyword) :: String.t()
end
