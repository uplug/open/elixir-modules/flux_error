# Changelog

## 0.0.1 - 2020-02-19

- **Added**:

  - The project is available on GitLab and Hex.

- **Notes**:

  - Minor changes (0.0.x) from the current version will be logged to this file.

  - When a major change is released (0.x.0 or x.0.0), the changelog of the
    previous major change will be grouped as a single change.
