# Flux Error

[![pipeline
status](https://gitlab.com/uplug/open/elixir-modules/flux_error/badges/master/pipeline.svg)](https://gitlab.com/uplug/open/elixir-modules/flux_error/commits/master)
[![coverage
report](https://gitlab.com/uplug/open/elixir-modules/flux_error/badges/master/coverage.svg)](https://gitlab.com/uplug/open/elixir-modules/flux_error/commits/master)

Exception module to improve error management.

## Usage

Add [Flux Error](https://hex.pm/packages/flux_error) as a dependency in your
`mix.exs` file:

```elixir
def deps do
  [{:flux_error, "~> 0.0.1"}]
end
```

`FluxError` describes how to use the module as `t:Exception.t/0`.

`FluxError.Messages` describes how to define the messages storage module.

`FluxError.DefaultMessages` is an example of a messages storage module

## Application Configuration

```elixir
import Config

# Default values
config :flux_error,
  messages_module: FluxError.DefaultMessages,
  default_options: []
```

### Configuration Options

- `:messages_module` - A `t:module/0` which extends `FluxError.Messages` and
  implements `c:FluxError.Messages.get_message/2`.

- `:default_options` - A `t:keyword/0` with the default message creation
  options.
