import Config

config :flux_error,
  messages_module: FluxError.DefaultMessages,
  default_options: []
